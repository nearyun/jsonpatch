package com.github;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String org = "{\"baz\": \"qux\",\"foo\": \"bar\"}";
		String str = "[{ \"op\": \"replace\", \"path\": \"/baz\", \"value\": \"boo\" },{ \"op\": \"add\", \"path\": \"/hello\", \"value\": [\"world\"] },{ \"op\": \"remove\", \"path\": \"/foo\"}]";
		final ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode orgNode = mapper.readTree(org);
			System.out.println(orgNode);
			
			JsonPatch patch = mapper.readValue(str, JsonPatch.class);
			System.out.println(patch);
			
			JsonNode patched = patch.apply(orgNode);
			System.out.println(patched);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JsonPatchException e) {
			e.printStackTrace();
		}
	}

}
